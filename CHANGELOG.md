# Changes

## 1.11.2

* Report current sheet if there are no running entries in `t now`.

## 1.11.1

* Change how migrate's help is organize to keep `t --help` clean.

## 1.11

* Finally implement the `migrate` command that converts from timetrap database
  to tiempo's format.
* Fix colors in chart formatter to work in dark and light backgrounds.

## 1.10

* Introduce `csvactivities` formatter.
* Update some dependencies.

## 1.9

* Text formatter now displays multiple sheets in a mixed view, allowing a better
  understanding of how time is distributed among activities of different sheets.
* Clap was upgraded to 4.2 and brings some changes to the look and feel of help
  dialogs.
* All display-family commands now accept mostly the same arguments. This added
  `--end` to some that didn't accept it.
* accept `now` as a time specification in arguments that receive a time
  specification.
* `t resume --interactive` now allows writing a new note directly there.
* `csv` formatter now allways includes database ids. Documentation for this
  arguments is improved in CLI.
* `activities` formatter that displays entries grouped by note with accumulated
  time and last time they were started.
* solve bug that displayed entries from yesterday as today in `t-list`.

## 1.8.2

* symlink manpage from t.1 to tiempo.1 in .tar.gz package and AUR bin and git
  packages.
* fix problem introduced in 1.7 that caused tiempo to crash if the default
  custom formatters directory didn't exist (this directory is not created
  automatically).

## 1.8

- try to create all parent directories when given a config path via environment
  variable
- read `TIEMPO_CONFIG` env var and give it priority over `TIMETRAP_CONFIG_FILE`.
- parse yesterday times like `yesterday at 15:20` for subcommands that accept a
  time.
- namespace for windows config changed from `tk.categulario.tiempo` to
  `xyz.categulario.tiempo`.

## 1.7

- parsed times in iso format now allow specifying the time with only one-digit
  hour, like: "2023-03-21 9:20"
- pass partial formatter names to --formatter (accepted by display and family).
  For example pass 't' and 'text' formatter will be used.

## 1.6

- `t now` displays a nicer message if no entries are running.
- `chart` formatter now shows the sheet name(s) that are being displayed.
- any-linux package now includes install script.

## 1.5.3

- added `--flat` option to `list` that displays only available sheet names one
  per line
- completions for bash, fish and zsh added to the package
- binary package structure changed

## 1.5

- First release to include the man page. Documentation is now published in
  https://tiempo.categulario.xyz

## 1.4.1

- Fix config not being passed to custom formatters

## 1.4.0

- Brand new `chart` formatter that shows your work history
- Fix bash-completions file
- Improve some output messages
- Fix timezone handling (in old databases) for --interactive
- Tell how to silence the timetrap warning as part of the warning :(
- Allow to set per-command default formatters

## 1.3.2

- Sort entries by most recent last in --interactive. Since most of the time you
  are likely recovering a recent entry you'll find it faster.

## 1.3.1

- Set correctly the description and dependencies for .deb package

## 1.3.0

- Archive entries by the total sum of hours passing t-archive the --time
  argument.

## 1.2.5

- From now on AUR packages (tiempo-git and tiempo-bin) are updated from CI
  (yay!)
- Upgrade dependency rusqlite to 0.28 because 0.25 was yanked (perhaps a
  security issue)

## 1.2.0

- Add `--interactive` to `resume` subcommand

## 1.1.0

- Custom formatters

## 1.0.0

- First functional release with most features already!
