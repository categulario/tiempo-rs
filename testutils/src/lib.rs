#[macro_export]
macro_rules! run_command {
    (config: $config:expr, args: $args:expr$(,)?) => {
        {
            use std::process::Stdio;

            $crate::run_command!(config: $config, args: $args, stdin: Stdio::null())
        }
    };

    (config: $config:expr, args: $args:expr, stdin: $stdin:expr$(,)?) => {
        {
            use std::process::{Command, Stdio};
            Command::new("target/debug/t")
                .env("TIMETRAP_CONFIG_FILE", $config)
                .args($args)
                .stdin($stdin)
                .stdout(Stdio::piped())
                .stderr(Stdio::piped())
                .spawn().unwrap()
                .wait_with_output().unwrap()
        }
    };
}
