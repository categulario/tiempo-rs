extern crate testutils;

use testutils::run_command;

use pretty_assertions::assert_str_eq;

#[test]
fn pass_all() {
    let output = run_command!{
        config: "tests/assets/ignoreme/config.toml",
        args: ["display", "all"],
    };

    assert_str_eq!(String::from_utf8_lossy(&output.stderr), "");
    assert_str_eq!(String::from_utf8_lossy(&output.stdout), "");
    assert_eq!(output.status.code().unwrap(), 0);
}
