use std::process::{Command, Stdio};

use pretty_assertions::assert_str_eq;

#[test]
fn tiempo_env_is_read() {
    let child  =Command::new("target/debug/t")
        .arg("configure")
        .env("TIEMPO_CONFIG", "tests/assets/config/tiempo.toml")
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn().unwrap();
    let output = child.wait_with_output().unwrap();

    assert_str_eq!(String::from_utf8_lossy(&output.stderr), "");
    assert_str_eq!(String::from_utf8_lossy(&output.stdout), "tests/assets/config/tiempo.toml\n");
    assert_eq!(output.status.code().unwrap(), 0);
}

#[test]
fn timetrap_env_is_read() {
    let child  =Command::new("target/debug/t")
        .arg("configure")
        .env("TIMETRAP_CONFIG_FILE", "tests/assets/config/timetrap.yml")
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn().unwrap();
    let output = child.wait_with_output().unwrap();

    assert_str_eq!(String::from_utf8_lossy(&output.stderr), "");
    assert_str_eq!(String::from_utf8_lossy(&output.stdout), "tests/assets/config/timetrap.yml\n");
    assert_eq!(output.status.code().unwrap(), 0);
}

#[test]
fn tiempo_env_has_priority() {
    let child  =Command::new("target/debug/t")
        .arg("configure")
        .env("TIMETRAP_CONFIG_FILE", "tests/assets/config/timetrap.yml")
        .env("TIEMPO_CONFIG", "tests/assets/config/tiempo.toml")
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn().unwrap();
    let output = child.wait_with_output().unwrap();

    assert_str_eq!(String::from_utf8_lossy(&output.stderr), "");
    assert_str_eq!(String::from_utf8_lossy(&output.stdout), "tests/assets/config/tiempo.toml\n");
    assert_eq!(output.status.code().unwrap(), 0);
}
