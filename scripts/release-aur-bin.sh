#!/bin/bash

set -e

# some useful variables
VERSION=${CI_COMMIT_TAG:1}
PROJECT_NAME=tiempo
PROJECT_BINARY=t
ARCHIVENAME=$PROJECT_NAME-$CI_COMMIT_TAG-x86_64.tar.gz

# get the sum from the artifacts
SUM=( `cat artifacts/$ARCHIVENAME.sum` )

# Generate the PKGBUILD
echo "# Maintainer: Abraham Toriz <categulario at gmail dot com>
pkgname=$PROJECT_NAME-bin
pkgver=$VERSION
pkgrel=1
pkgdesc='A command line time tracking application'
arch=('x86_64')
url='https://gitlab.com/categulario/tiempo-rs'
license=('GPL3')
depends=()
optdepends=('sqlite: for manually editing the database')
provides=('$PROJECT_NAME')
conflicts=('$PROJECT_NAME')
source=(\"https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/packages/generic/$CI_COMMIT_TAG/$CI_COMMIT_TAG/$PROJECT_NAME-v\${pkgver}-x86_64.tar.gz\")
sha256sums=('$SUM')

package() {
    cd \"\$srcdir/build\"
    install -Dm755 bin/$PROJECT_BINARY \"\$pkgdir\"/usr/bin/$PROJECT_BINARY

    install -Dm644 share/doc/$PROJECT_NAME/README.md \"\$pkgdir\"/usr/share/doc/$PROJECT_NAME/README.md
    install -Dm644 share/doc/$PROJECT_NAME/LICENSE \"\$pkgdir\"/usr/share/doc/$PROJECT_NAME/LICENSE
    install -Dm644 share/doc/$PROJECT_NAME/CHANGELOG.md \"\$pkgdir\"/usr/share/doc/$PROJECT_NAME/CHANGELOG.md
    install -Dm644 share/man/man1/$PROJECT_NAME.1.gz \"\$pkgdir\"/usr/share/man/man1/$PROJECT_NAME.1.gz
    install -Dm644 share/bash-completion/completions/t \"\$pkgdir\"/usr/share/bash-completion/completions/t
    install -Dm644 share/fish/vendor_completions.d/t.fish \"\$pkgdir\"/usr/share/fish/vendor_completions.d/t.fish
    install -Dm644 share/zsh/site-functions/_t \"\$pkgdir\"/usr/share/zsh/site-functions/_t

    ln -s \"\$pkgdir\"/usr/share/man/man1/$PROJECT_NAME.1.gz \"\$pkgdir\"/usr/share/man/man1/t.1.gz
}
" | tee PKGBUILD > /dev/null

makepkg --printsrcinfo > .SRCINFO
