use chrono::{DateTime, Utc, Duration};
use serde::Serialize;

#[derive(Debug, Clone, PartialEq, Eq, Serialize)]
pub struct Entry {
    pub id: u64,
    pub note: Option<String>,
    pub start: DateTime<Utc>,
    pub end: Option<DateTime<Utc>>,
    pub sheet: String,
}

#[derive(Debug)]
pub struct Meta {
    pub id: u64,
    pub key: String,
    pub value: String,
}

impl Entry {
    #[cfg(test)]
    pub fn new_sample(id: u64, start: DateTime<Utc>, end: Option<DateTime<Utc>>) -> Entry {
        Entry {
            id,
            note: Some(format!("entry {}", id)),
            start, end,
            sheet: "default".into(),
        }
    }

    pub fn with_sheet(self, sheet: &str) -> Entry {
        Entry {
            sheet: sheet.into(),
            ..self
        }
    }

    pub fn timespan(&self) -> Option<Duration> {
        self.end.map(|e| e - self.start)
    }

    // returns the number of hours of this entry as decimal. If entry is
    // unfinished return its elapsed time so far.
    pub fn hours(&self, now: DateTime<Utc>) -> f64 {
        let d = self.end.unwrap_or(now) - self.start;

        d.num_hours() as f64 + (d.num_minutes() % 60) as f64 / 60.0 + (d.num_seconds() % 60) as f64 / 3600.0
    }
}
