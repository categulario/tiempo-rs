use std::io::{BufRead, Write};
use std::path::PathBuf;

use clap::Args;

use crate::database::Database;
use crate::error::Result;
use crate::commands::{Command, Facts};
use crate::config::WeekDay;
use crate::formatters::Formatter;
use crate::io::Streams;

/// Configure tiempo in-place. If no arguments are given it just prints the path
/// to the config file in use. Only arguments passed are set in the config file.
#[derive(Default, Args)]
pub struct Cli {
    /// The file path of the sqlite database
    #[arg(long, value_name="PATH")]
    database_file: Option<PathBuf>,

    /// The duration of time to use for rounding with the -r flag. Default: 900
    /// (15 minutes)
    #[arg(long, value_name="SECONDS")]
    round_in_seconds: Option<u32>,

    /// Delimiter used when appending notes via t edit --append. Default: ' '
    /// (space)
    #[arg(long, value_name="DELIMITER")]
    append_notes_delimiter: Option<String>,

    /// Use this directory to search for user defined fomatters. Pass multiple
    /// times to set multiple directories. It overrides the previous values.
    #[arg(long, value_name="PATH")]
    formatter_search_path: Option<Vec<PathBuf>>,

    /// The format to use when display is invoked without a `--format` option.
    /// Default: 'text'
    #[arg(long, value_name="FORMATTER")]
    default_formatter: Option<Formatter>,

    /// Pass 'yes' to always ask for a note when starting an entry or 'no' to
    /// allow entries without note.
    #[arg(long)]
    require_note: Option<bool>,

    /// Command to launch notes editor. Default: $EDITOR
    #[arg(long, value_name="EDITOR")]
    note_editor: Option<String>,

    /// The day of the week to use as the start of the week for t week. Default: monday
    #[arg(long, value_name="DAY")]
    week_start: Option<WeekDay>,

    /// How many unique previous notes to show when selecting interactively
    #[arg(long, value_name="N")]
    interactive_entries: Option<usize>,
}

impl Cli {
    /// returns true only if no argument was passed other that possibly --id.
    /// This means that an edit was requested without specifying what to edit.
    fn none_given(&self) -> bool {
        !(
            self.database_file.is_some() ||
            self.round_in_seconds.is_some() ||
            self.append_notes_delimiter.is_some() ||
            self.formatter_search_path.is_some() ||
            self.default_formatter.is_some() ||
            self.require_note.is_some() ||
            self.note_editor.is_some() ||
            self.week_start.is_some() ||
            self.interactive_entries.is_some()
        )
    }
}

impl Command for Cli {
    fn handle<D, I, O, E>(self, streams: &mut Streams<D, I, O, E>, facts: &Facts) -> Result<()>
    where
        D: Database,
        I: BufRead,
        O: Write,
        E: Write,
    {
        if self.none_given() {
            if let Some(path) = facts.config.path.as_deref() {
                writeln!(streams.out, "{}", path.display())?;
            } else {
                writeln!(streams.out, "Config file is in memory")?;
            }

            Ok(())
        } else {
            let mut new_config = facts.config.clone();

            if let Some(path) = self.database_file {
                new_config.database_file = path;
            }

            if let Some(val) = self.round_in_seconds {
                new_config.round_in_seconds = val;
            }

            if let Some(val) = self.append_notes_delimiter {
                new_config.append_notes_delimiter = val;
            }

            if let Some(val) = self.formatter_search_path {
                new_config.formatter_search_paths = val;
            }

            if let Some(val) = self.default_formatter {
                new_config.default_formatter = val;
            }

            if let Some(val) = self.require_note {
                new_config.require_note = val;
            }

            if let Some(val) = self.note_editor {
                new_config.note_editor = Some(val);
            }

            if let Some(val) = self.week_start {
                new_config.week_start = val;
            }

            if let Some(n) = self.interactive_entries {
                new_config.interactive_entries = n;
            }

            if let Some(path) = facts.config.path.as_deref() {
                let output = new_config.write(path)?;

                writeln!(streams.out, "Your new config:\n")?;

                streams.out.write_all(output.as_bytes())?;
            } else {
                writeln!(streams.out, "Your config file is in memory and cannot be written to")?;
            }

            Ok(())
        }
    }
}
