use std::io::{BufRead, Write};

use clap::Args;
use chrono::{DateTime, Utc, Local, Duration, Weekday, Datelike, Timelike};

use crate::error::Result;
use crate::database::Database;
use crate::config::WeekDay;
use crate::io::Streams;
use crate::commands::display::{DisplayOpts, CommonCli, entries_for_display};

use super::{Command, Facts};

trait AsNum {
    fn as_num(&self) -> i64;
}

// Tiempo's WeekDay
impl AsNum for WeekDay {
    fn as_num(&self) -> i64 {
        match &self {
            WeekDay::Monday => 1,
            WeekDay::Tuesday => 2,
            WeekDay::Wednesday => 3,
            WeekDay::Thursday => 4,
            WeekDay::Friday => 5,
            WeekDay::Saturday => 6,
            WeekDay::Sunday => 7,
        }
    }
}

// Chrono's Weekday
impl AsNum for Weekday {
    fn as_num(&self) -> i64 {
        match &self {
            Weekday::Mon => 1,
            Weekday::Tue => 2,
            Weekday::Wed => 3,
            Weekday::Thu => 4,
            Weekday::Fri => 5,
            Weekday::Sat => 6,
            Weekday::Sun => 7,
        }
    }
}

/// Given a local datetime, returns the time of the previous week_start's start
fn prev_day(now: DateTime<Local>, week_start: WeekDay) -> DateTime<Utc> {
    let begining = match now.weekday().as_num() - week_start.as_num() {
        0 => now,
        num if num > 0 => now - Duration::days(num),
        num if num < 0 => now - Duration::days(7 - num.abs()),
        _ => unreachable!(),
    };

    begining
        .with_hour(0).unwrap()
        .with_minute(0).unwrap()
        .with_second(0).unwrap()
        .with_nanosecond(0).unwrap()
        .with_timezone(&Utc)
}

#[derive(Default, Args)]
pub struct Cli {
    #[command(flatten)]
    common: CommonCli,
}

impl Command for Cli {
    fn handle<D, I, O, E>(self, streams: &mut Streams<D, I, O, E>, facts: &Facts) -> Result<()>
    where
        D: Database,
        I: BufRead,
        O: Write,
        E: Write,
    {
        let start = prev_day(facts.now.with_timezone(&Local), facts.config.week_start);

        entries_for_display(
            DisplayOpts {
                start: Some(start),
                end: self.common.end,
                sheet: self.common.sheet,
                format: self.common.format.unwrap_or_else(|| {
                    facts
                        .config.commands.week.default_formatter.as_ref()
                        .map(|d| d.to_string())
                        .unwrap_or_else(|| facts.config.default_formatter.to_string())
                }),
                ids: self.common.ids,
                sort: self.common.sort,
                grep: self.common.grep,
            },
            streams,
            facts
        )
    }
}

#[cfg(test)]
mod tests {
    use chrono::TimeZone;

    use crate::config::{Config, CommandsSettings, BaseCommandSettings};
    use crate::formatters::Formatter;

    use super::*;

    #[test]
    fn test_prev_day() {
        // starting a saturday
        let now = Local.with_ymd_and_hms(2021, 7, 10, 18, 31, 0).unwrap();

        assert_eq!(prev_day(now, WeekDay::Monday), Local.with_ymd_and_hms(2021, 7, 5, 0, 0, 0).unwrap().with_timezone(&Utc));
        assert_eq!(prev_day(now, WeekDay::Tuesday), Local.with_ymd_and_hms(2021, 7, 6, 0, 0, 0).unwrap().with_timezone(&Utc));
        assert_eq!(prev_day(now, WeekDay::Wednesday), Local.with_ymd_and_hms(2021, 7, 7, 0, 0, 0).unwrap().with_timezone(&Utc));
        assert_eq!(prev_day(now, WeekDay::Thursday), Local.with_ymd_and_hms(2021, 7, 8, 0, 0, 0).unwrap().with_timezone(&Utc));
        assert_eq!(prev_day(now, WeekDay::Friday), Local.with_ymd_and_hms(2021, 7, 9, 0, 0, 0).unwrap().with_timezone(&Utc));
        assert_eq!(prev_day(now, WeekDay::Saturday), Local.with_ymd_and_hms(2021, 7, 10, 0, 0, 0).unwrap().with_timezone(&Utc));
        assert_eq!(prev_day(now, WeekDay::Sunday), Local.with_ymd_and_hms(2021, 7, 4, 0, 0, 0).unwrap().with_timezone(&Utc));
    }

    #[test]
    fn respect_default_formatter() {
        std::env::set_var("TZ", "CST+6");

        let args = Cli::default();
        let mut streams = Streams::fake(b"");
        let now = Utc.with_ymd_and_hms(2021, 7, 1, 10, 0, 0).unwrap();
        let facts = Facts::new().with_config(Config {
            default_formatter: Formatter::Ids,
            ..Default::default()
        }).with_now(now);

        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 6, 30, 10, 0, 0).unwrap(), None, Some("hola".into()), "default").unwrap();
        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 6, 30, 10, 10, 0).unwrap(), None, Some("hola".into()), "default").unwrap();

        args.handle(&mut streams, &facts).unwrap();

        assert_eq!(&String::from_utf8_lossy(&streams.out), "1 2\n");
        assert_eq!(String::from_utf8_lossy(&streams.err), "");
    }

    #[test]
    fn respect_command_default_formatter() {
        std::env::set_var("TZ", "CST+6");

        let args = Cli::default();
        let mut streams = Streams::fake(b"");
        let now = Utc.with_ymd_and_hms(2021, 7, 1, 10, 0, 0).unwrap();
        let facts = Facts::new().with_config(Config {
            commands: CommandsSettings {
                week: BaseCommandSettings {
                    default_formatter: Some(Formatter::Ids),
                },
                ..Default::default()
            },
            ..Default::default()
        }).with_now(now);

        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 6, 30, 10, 0, 0).unwrap(), None, Some("hola".into()), "default").unwrap();
        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 6, 30, 10, 10, 0).unwrap(), None, Some("hola".into()), "default").unwrap();

        args.handle(&mut streams, &facts).unwrap();

        assert_eq!(&String::from_utf8_lossy(&streams.out), "1 2\n");
        assert_eq!(String::from_utf8_lossy(&streams.err), "");
    }
}
