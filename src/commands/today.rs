use std::io::{BufRead, Write};

use clap::Args;
use chrono::{Utc, Local, Timelike};

use crate::error::Result;
use crate::database::Database;
use crate::io::Streams;
use crate::commands::display::{DisplayOpts, CommonCli, entries_for_display};

use super::{Command, Facts};

/// Display entries that started today
#[derive(Default, Args)]
pub struct Cli {
    #[command(flatten)]
    common: CommonCli,
}

impl Command for Cli {
    fn handle<D, I, O, E>(self, streams: &mut Streams<D, I, O, E>, facts: &Facts) -> Result<()>
    where
        D: Database,
        I: BufRead,
        O: Write,
        E: Write,
    {
        let start = Some(facts.now
            .with_timezone(&Local)
            .with_hour(0).unwrap()
            .with_minute(0).unwrap()
            .with_second(0).unwrap()
            .with_nanosecond(0).unwrap()
            .with_timezone(&Utc));

        entries_for_display(
            DisplayOpts {
                start,
                end: self.common.end,
                sheet: self.common.sheet,
                format: self.common.format.unwrap_or_else(|| {
                    facts
                        .config.commands.today.default_formatter.as_ref()
                        .map(|d| d.to_string())
                        .unwrap_or_else(|| facts.config.default_formatter.to_string())
                }),
                ids: self.common.ids,
                sort: self.common.sort,
                grep: self.common.grep,
            },
            streams,
            facts
        )
    }
}

#[cfg(test)]
mod tests {
    use chrono::TimeZone;

    use crate::config::{Config, CommandsSettings, BaseCommandSettings};
    use crate::formatters::Formatter;

    use super::*;

    #[test]
    fn respect_default_formatter() {
        std::env::set_var("TZ", "CST+6");

        let args = Cli::default();
        let mut streams = Streams::fake(b"");
        let facts = Facts::new().with_config(Config {
            default_formatter: Formatter::Ids,
            ..Default::default()
        }).with_now(Utc.with_ymd_and_hms(2021, 6, 30, 11, 0, 0).unwrap());

        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 6, 30, 10, 0, 0).unwrap(), None, Some("hola".into()), "default").unwrap();
        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 6, 30, 10, 10, 0).unwrap(), None, Some("hola".into()), "default").unwrap();

        args.handle(&mut streams, &facts).unwrap();

        assert_eq!(&String::from_utf8_lossy(&streams.out), "1 2\n");
        assert_eq!(String::from_utf8_lossy(&streams.err), "");
    }

    #[test]
    fn respect_command_default_formatter() {
        std::env::set_var("TZ", "CST+6");

        let args = Cli::default();
        let mut streams = Streams::fake(b"");
        let facts = Facts::new().with_config(Config {
            commands: CommandsSettings {
                today: BaseCommandSettings {
                    default_formatter: Some(Formatter::Ids),
                },
                ..Default::default()
            },
            ..Default::default()
        }).with_now(Utc.with_ymd_and_hms(2021, 6, 30, 11, 0, 0).unwrap());

        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 6, 30, 10, 0, 0).unwrap(), None, Some("hola".into()), "default").unwrap();
        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 6, 30, 10, 10, 0).unwrap(), None, Some("hola".into()), "default").unwrap();

        args.handle(&mut streams, &facts).unwrap();

        assert_eq!(&String::from_utf8_lossy(&streams.out), "1 2\n");
        assert_eq!(String::from_utf8_lossy(&streams.err), "");
    }
}
