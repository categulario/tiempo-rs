use std::io::{BufRead, Write};
use std::str::FromStr;
use std::fmt;

use clap::Args;
use chrono::{DateTime, Utc, Local, Datelike, TimeZone};

use crate::error::{Result, Error};
use crate::database::Database;
use crate::io::Streams;
use crate::commands::display::{DisplayOpts, CommonCli, entries_for_display};

use super::{Command, Facts};

/// Given a local datetime, returns the time when the month it belongs started
fn beginning_of_month(time: DateTime<Local>) -> DateTime<Utc> {
    time.date_naive().with_day(1).unwrap().and_hms_opt(0, 0, 0).unwrap().and_local_timezone(Utc).unwrap()
}

/// Given a datetime compute the time where the previous_month started in UTC
fn beginning_of_previous_month(time: DateTime<Local>) -> DateTime<Utc> {
    match time.month() {
        1 => {
            Local.with_ymd_and_hms(time.year()-1, 12, 1, 0, 0, 0).unwrap().with_timezone(&Utc)
        }
        n => Local.with_ymd_and_hms(time.year(), n-1, 1, 0, 0, 0).unwrap().with_timezone(&Utc)
    }
}

#[derive(Default, Copy, Clone)]
enum MonthSpec {
    Last,
    #[default]
    This,
    Month(u32),
}

impl fmt::Display for MonthSpec {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        match self {
            MonthSpec::Last => write!(formatter, "last"),
            MonthSpec::This => write!(formatter, "current"),
            MonthSpec::Month(1) => write!(formatter, "january"),
            MonthSpec::Month(2) => write!(formatter, "february"),
            MonthSpec::Month(3) => write!(formatter, "march"),
            MonthSpec::Month(4) => write!(formatter, "april"),
            MonthSpec::Month(5) => write!(formatter, "may"),
            MonthSpec::Month(6) => write!(formatter, "june"),
            MonthSpec::Month(7) => write!(formatter, "july"),
            MonthSpec::Month(8) => write!(formatter, "august"),
            MonthSpec::Month(9) => write!(formatter, "september"),
            MonthSpec::Month(10) => write!(formatter, "october"),
            MonthSpec::Month(11) => write!(formatter, "november"),
            MonthSpec::Month(12) => write!(formatter, "december"),
            MonthSpec::Month(_) => unreachable!(),
        }
    }
}

impl FromStr for MonthSpec {
    type Err = Error;

    fn from_str(s: &str) -> Result<MonthSpec> {
        match s.trim().to_lowercase().as_str() {
            "this" | "current" => Ok(MonthSpec::This),
            "last" => Ok(MonthSpec::Last),
            "jan" | "january" => Ok(MonthSpec::Month(1)),
            "feb" | "february" => Ok(MonthSpec::Month(2)),
            "mar" | "march" => Ok(MonthSpec::Month(3)),
            "apr" | "april" => Ok(MonthSpec::Month(4)),
            "may" => Ok(MonthSpec::Month(5)),
            "jun" | "june" => Ok(MonthSpec::Month(6)),
            "jul" | "july" => Ok(MonthSpec::Month(7)),
            "aug" | "august" => Ok(MonthSpec::Month(8)),
            "sep" | "september" => Ok(MonthSpec::Month(9)),
            "oct" | "october" => Ok(MonthSpec::Month(10)),
            "nov" | "november" => Ok(MonthSpec::Month(11)),
            "dic" | "december" => Ok(MonthSpec::Month(12)),
            _ => Err(Error::InvalidMonthSpec(s.into())),
        }
    }
}

/// Display entries starting this month
#[derive(Default, Args)]
pub struct Cli {
    /// Include entries of the specified month instead of the current month
    #[arg(short, long, value_name="MONTH", default_value_t)]
    month: MonthSpec,

    #[command(flatten)]
    common: CommonCli,
}

impl Command for Cli {
    fn handle<D, I, O, E>(self, streams: &mut Streams<D, I, O, E>, facts: &Facts) -> Result<()>
    where
        D: Database,
        I: BufRead,
        O: Write,
        E: Write,
    {
        let now = facts.now;
        let (start, end) = match self.month {
            MonthSpec::This => (beginning_of_month(now.with_timezone(&Local)), now),
            MonthSpec::Last => {
                (beginning_of_previous_month(now.with_timezone(&Local)), beginning_of_month(now.with_timezone(&Local)))
            },
            MonthSpec::Month(month) => {
                if month < now.month() {
                    // the specified month is in the current year
                    (
                        Local.with_ymd_and_hms(now.year(), month, 1, 0, 0, 0).unwrap().with_timezone(&Utc),
                        if month < 12 {
                            Local.with_ymd_and_hms(now.year(), month+1, 1, 0, 0, 0).unwrap().with_timezone(&Utc)
                        } else {
                            Local.with_ymd_and_hms(now.year()+1, 1, 1, 0, 0, 0).unwrap().with_timezone(&Utc)
                        }
                    )
                } else {
                    // use previous year
                    (
                        Local.with_ymd_and_hms(now.year() - 1, month, 1, 0, 0, 0).unwrap().with_timezone(&Utc),
                        if month < 12 {
                            Local.with_ymd_and_hms(now.year() - 1, month + 1, 1, 0, 0, 0).unwrap().with_timezone(&Utc)
                        } else {
                            Local.with_ymd_and_hms(now.year(), 1, 1, 0, 0, 0).unwrap().with_timezone(&Utc)
                        }
                    )
                }
            },
        };

        entries_for_display(
            DisplayOpts {
                start: Some(start),
                end: Some(end),
                sheet: self.common.sheet,
                format: self.common.format.unwrap_or_else(|| {
                    facts
                        .config.commands.month.default_formatter.as_ref()
                        .map(|d| d.to_string())
                        .unwrap_or_else(|| facts.config.default_formatter.to_string())
                }),
                ids: self.common.ids,
                sort: self.common.sort,
                grep: self.common.grep,
            },
            streams,
            facts
        )
    }
}

#[cfg(test)]
mod tests {
    use crate::config::{Config, CommandsSettings, BaseCommandSettings};
    use crate::formatters::Formatter;

    use super::*;

    #[test]
    fn respect_default_formatter() {
        std::env::set_var("TZ", "CST+6");

        let args = Cli::default();
        let mut streams = Streams::fake(b"");
        let now = Utc.with_ymd_and_hms(2021, 6, 30, 11, 0, 0).unwrap();
        let facts = Facts::new().with_config(Config {
            default_formatter: Formatter::Ids,
            ..Default::default()
        }).with_now(now);

        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 6, 30, 10, 0, 0).unwrap(), None, Some("hola".into()), "default").unwrap();
        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 6, 30, 10, 10, 0).unwrap(), None, Some("hola".into()), "default").unwrap();

        args.handle(&mut streams, &facts).unwrap();

        assert_eq!(&String::from_utf8_lossy(&streams.out), "1 2\n");
        assert_eq!(String::from_utf8_lossy(&streams.err), "");
    }

    #[test]
    fn respect_command_default_formatter() {
        std::env::set_var("TZ", "CST+6");

        let args = Cli::default();
        let mut streams = Streams::fake(b"");
        let now = Utc.with_ymd_and_hms(2021, 6, 30, 11, 0, 0).unwrap();
        let facts = Facts::new().with_config(Config {
            commands: CommandsSettings {
                month: BaseCommandSettings {
                    default_formatter: Some(Formatter::Ids),
                },
                ..Default::default()
            },
            ..Default::default()
        }).with_now(now);

        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 6, 30, 10, 0, 0).unwrap(), None, Some("hola".into()), "default").unwrap();
        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 6, 30, 10, 10, 0).unwrap(), None, Some("hola".into()), "default").unwrap();

        args.handle(&mut streams, &facts).unwrap();

        assert_eq!(&String::from_utf8_lossy(&streams.out), "1 2\n");
        assert_eq!(String::from_utf8_lossy(&streams.err), "");
    }
}
