use std::io::{BufRead, Write};

use clap::Args;

use crate::database::Database;
use crate::error::Result;
use crate::commands::{Command, Facts};
use crate::commands::list::Cli as ListCli;
use crate::io::Streams;

/// Change active timesheet or list existing timesheets
#[derive(Default, Args)]
pub struct Cli {
    /// The sheet to switch to. Use - to quickly switch to the previous sheet
    #[arg(value_name="SHEET")]
    pub sheet: Option<String>,
}

impl Command for Cli {
    fn handle<D, I, O, E>(self, streams: &mut Streams<D, I, O, E>, facts: &Facts) -> Result<()>
    where
        D: Database,
        I: BufRead,
        O: Write,
        E: Write,
    {
        if let Some(sheet) = self.sheet {
            let current_sheet = streams.db.current_sheet()?;

            // sheet given, switch to it
            let move_to = if sheet == "-" {
                if let Some(move_to) = streams.db.last_sheet()? {
                    move_to
                } else {
                    writeln!(streams.out, "No previous sheet to move to. Staying on '{}'.
Hint: remember that giving - (a dash) as argument to t sheet switches to the last active sheet", current_sheet)?;

                    return Ok(());
                }
            } else if sheet == current_sheet {
                writeln!(streams.out, "Already on sheet '{}'", sheet)?;

                return Ok(());
            } else {
                sheet
            };

            streams.db.set_last_sheet(&current_sheet)?;
            streams.db.set_current_sheet(&move_to)?;

            writeln!(streams.out, "Switching to sheet '{}'", move_to)?;

            Ok(())
        } else {
            // call list
            ListCli::default().handle(streams, facts)
        }
    }
}

#[cfg(test)]
mod tests {
    use pretty_assertions::assert_eq;

    use super::*;

    #[test]
    fn switch_to_sheet() {
        let args = Cli {
            sheet: Some("new_sheet".into()),
        };
        let mut streams = Streams::fake(b"");
        let facts = Facts::new();

        args.handle(&mut streams, &facts).unwrap();

        assert_eq!(streams.db.current_sheet().unwrap(), "new_sheet");
        assert_eq!(&String::from_utf8_lossy(&streams.out), "Switching to sheet 'new_sheet'\n");
        assert_eq!(&String::from_utf8_lossy(&streams.err), "");
    }

    #[test]
    fn switch_to_sheet_already_in() {
        let args = Cli {
            sheet: Some("foo".into()),
        };
        let mut streams = Streams::fake(b"");
        let facts = Facts::new();

        streams.db.set_current_sheet("foo").unwrap();

        args.handle(&mut streams, &facts).unwrap();

        assert_eq!(streams.db.current_sheet().unwrap(), "foo");
        assert_eq!(&String::from_utf8_lossy(&streams.out), "Already on sheet 'foo'\n");
        assert_eq!(&String::from_utf8_lossy(&streams.err), "");
    }

    #[test]
    fn switch_to_last_sheet() {
        let args = Cli {
            sheet: Some("-".into()),
        };
        let mut streams = Streams::fake(b"");
        let facts = Facts::new();

        streams.db.set_current_sheet("foo").unwrap();
        streams.db.set_last_sheet("var").unwrap();

        args.handle(&mut streams, &facts).unwrap();

        assert_eq!(streams.db.current_sheet().unwrap(), "var");
        assert_eq!(streams.db.last_sheet().unwrap().unwrap(), "foo");
        assert_eq!(&String::from_utf8_lossy(&streams.out), "Switching to sheet 'var'\n");
        assert_eq!(&String::from_utf8_lossy(&streams.err), "");
    }
}
