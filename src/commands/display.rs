use std::io::{BufRead, Write};
use std::str::FromStr;
use std::fmt;

use clap::Args;
use chrono::{DateTime, Utc};
use regex::Regex;

use crate::error::{Result, Error};
use crate::database::Database;
use crate::formatters::{MatchResult, autocomplete, get_formatters};
use crate::timeparse::parse_time;
use crate::regex::parse_regex;
use crate::old::{entries_or_warning, time_or_warning, warn_if_needed};
use crate::io::Streams;

use super::{Command, Facts};

// ----------------------------------------------------------------
// Things that are general to all commands that display in some way
// ----------------------------------------------------------------

/// Different kinds of sheet specification. This serves de purpose of being able
/// to pass 'all' or 'full' as sheet names and have special functionality.
#[derive(Clone)]
pub enum Sheet {
    All,
    Full,
    Sheet(String),
}

impl FromStr for Sheet {
    type Err = Error;

    fn from_str(name: &str) -> Result<Sheet> {
        Ok(match name {
            "all" => Sheet::All,
            "full" => Sheet::Full,
            name => Sheet::Sheet(name.into()),
        })
    }
}

/// Options to pass to the entries_for_display function.
#[derive(Default)]
pub struct DisplayOpts {
    pub start: Option<DateTime<Utc>>,
    pub end: Option<DateTime<Utc>>,
    pub sheet: Option<Sheet>,
    pub format: String,
    pub ids: bool,
    pub sort: Sort,
    pub grep: Option<Regex>,
}

/// Logic for displaying entries common to all display-family subcommands.
pub fn entries_for_display<D, I, O, E>(
    opts: DisplayOpts,
    streams: &mut Streams<D, I, O, E>,
    facts: &Facts,
) -> Result<()>
where
    D: Database,
    I: BufRead,
    O: Write,
    E: Write,
{
    let start = opts.start.map(|s| time_or_warning(s, &streams.db)).transpose()?.map(|s| s.0);
    let end = opts.end.map(|e| time_or_warning(e, &streams.db)).transpose()?.map(|e| e.0);

    let mut entries = match opts.sheet {
        Some(Sheet::All) => streams.db.entries_all_visible(start, end)?,
        Some(Sheet::Full) => streams.db.entries_full(start, end)?,
        Some(Sheet::Sheet(name)) => streams.db.entries_by_sheet(&name, start, end)?,
        None => {
            let current_sheet = streams.db.current_sheet()?;

            streams.db.entries_by_sheet(&current_sheet, start, end)?
        }
    };

    if let Some(re) = opts.grep {
        entries.retain(|e| re.is_match(&e.note.clone().unwrap_or_default()));
    }

    let (entries, needs_warning) = entries_or_warning(entries, &streams.db)?;

    let formatter = match autocomplete(get_formatters(&facts.config)?, &opts.format) {
        MatchResult::Found(f) => f,
        MatchResult::NotFound(s) => {
            return Err(Error::FormatterNotFound {
                name: s,
                paths: facts.config.formatter_search_paths.clone(),
                config_at: facts.config.path.clone().unwrap_or_else(|| "in memory".into()),
            });
        },
        MatchResult::Ambiguous(matches) => {
            return Err(Error::AmbiguousFormatter {
                given: opts.format,
                matches,
            });
        }
    };

    formatter.print_formatted(
        entries,
        &mut streams.out,
        &mut streams.err,
        facts,
        opts.ids,
        opts.sort,
    )?;

    warn_if_needed(&mut streams.err, needs_warning, &facts.env)?;

    Ok(())
}

// ------------------------------------
// The actual implementation of display
// ------------------------------------

#[derive(Copy, Clone, Default)]
pub enum Sort {
    NoteAsc,
    NoteDsc,
    TimeAsc,
    TimeDsc,
    LastStartedAsc,
    #[default]
    LastStartedDsc,
}

impl FromStr for Sort {
    type Err = Error;

    fn from_str(s: &str) -> Result<Sort> {
        Ok(match s {
            "note" => Sort::NoteAsc,
            "-note" => Sort::NoteDsc,
            "time" => Sort::TimeAsc,
            "-time" => Sort::TimeDsc,
            "last_started" => Sort::LastStartedAsc,
            "-last_started" => Sort::LastStartedDsc,
            _ => return Err(Error::GenericFailure(
                "Use 'note', 'time' or 'last_started'. Prepend a '-' to sort in descending order.".into()
            )),
        })
    }
}

impl fmt::Display for Sort {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Sort::NoteAsc => write!(formatter, "note"),
            Sort::NoteDsc => write!(formatter, "-note"),
            Sort::TimeAsc => write!(formatter, "time"),
            Sort::TimeDsc => write!(formatter, "-time"),
            Sort::LastStartedAsc => write!(formatter, "last_started"),
            Sort::LastStartedDsc => write!(formatter, "-last_started"),
        }
    }
}

/// Arguments shared by all display-family commands
#[derive(Default, Args)]
pub struct CommonCli {
    /// Print database ids in 'text' formatter. Useful to edit entries with
    /// `t edit`.
    #[arg(short='v', long)]
    pub ids: bool,

    /// Include entries that start on this date or earlier
    #[arg(short, long, value_name="TIME", value_parser=parse_time)]
    pub end: Option<DateTime<Utc>>,

    /// The output format. Valid built-in formats are chart, text, ical, csv,
    /// json, ids, activities and csvactivities. Documentation on defining
    /// custom formats can be found at https://tiempo.categulario.xyz or the man
    /// page included with the installation.
    #[arg(short, long, value_name="FORMAT")]
    pub format: Option<String>,

    /// Sort entries by this in the `activities` and `csvactivities` formatter.
    /// Options are `text`, `time` and `last_started`. Add a `-` (minus sign)
    /// before the criteria to make it a descending order. Default is
    /// `-last_started`.
    #[arg(long, value_name="COLUMN", allow_hyphen_values=true, default_value_t)]
    pub sort: Sort,

    /// Only include entries whose note matches this regular expression
    #[arg(short, long, value_name="REGEXP", value_parser=parse_regex)]
    pub grep: Option<Regex>,

    /// The sheet to display. Pass 'all' to see entries from all sheets or
    /// 'full' to see hidden entries
    #[arg(value_name="SHEET")]
    pub sheet: Option<Sheet>,
}

/// Display the current timesheet or a specific one. Pass `all' as SHEET to
/// display all unarchived sheets or `full' to display archived and unarchived
/// sheets.
#[derive(Default, Args)]
pub struct Cli {
    /// Include entries that start on this date or later
    #[arg(short, long, value_name="TIME", value_parser=parse_time)]
    start: Option<DateTime<Utc>>,

    #[command(flatten)]
    common: CommonCli,
}

impl Command for Cli {
    fn handle<D, I, O, E>(self, streams: &mut Streams<D, I, O, E>, facts: &Facts) -> Result<()>
    where
        D: Database,
        I: BufRead,
        O: Write,
        E: Write,
    {
        entries_for_display(
            DisplayOpts {
                start: self.start,
                end: self.common.end,
                sheet: self.common.sheet,
                format: self.common.format.unwrap_or_else(|| {
                    facts
                        .config.commands.display.default_formatter.as_ref()
                        .map(|d| d.to_string())
                        .unwrap_or_else(|| facts.config.default_formatter.to_string())
                }),
                ids: self.common.ids,
                sort: self.common.sort,
                grep: self.common.grep,
            },
            streams,
            facts
        )
    }
}

#[cfg(test)]
mod tests {
    use chrono::TimeZone;
    use pretty_assertions::{assert_eq, assert_str_eq};

    use crate::database::SqliteDatabase;
    use crate::config::{Config, CommandsSettings, BaseCommandSettings};
    use crate::formatters::{Formatter, tests::get_config};
    use crate::commands::display::DisplayOpts;

    use super::*;

    #[test]
    fn display_as_local_time_if_previous_version() {
        std::env::set_var("TZ", "CST+6");

        let args = Cli::default();
        let mut streams = Streams::fake(b"").with_db(
            SqliteDatabase::from_path("tests/assets/db/test_old_db.db").unwrap()
        );
        let facts = Facts::new();

        args.handle(&mut streams, &facts).unwrap();

        assert_str_eq!(&String::from_utf8_lossy(&streams.out), "    Day                Start      End        Duration Notes
    Tue Jun 29, 2021   06:26:49 - 07:26:52    1:00:03 lets do some rust
                                              1:00:03
-----------------------------------------------------------------------
    Total                                     1:00:03

Timesheet: default
");

        assert_str_eq!(
            String::from_utf8_lossy(&streams.err),
            "[WARNING] You are using the old timetrap format, it is advised that you update your database using t migrate. To supress this warning set TIEMPO_SUPRESS_TIMETRAP_WARNING=1\n"
        );
    }

    #[test]
    fn filter_by_start() {
        let args = Cli {
            common: CommonCli {
                format: Some(Formatter::Csv.to_string()),
                ..Default::default()
            },
            start: Some(Utc.with_ymd_and_hms(2021, 6, 30, 10, 5, 0).unwrap()),
        };
        let mut streams = Streams::fake(b"");
        let facts = Facts::new();

        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 6, 30, 10, 0, 0).unwrap(), None, Some("hola".into()), "default").unwrap();
        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 6, 30, 10, 10, 0).unwrap(), None, Some("hola".into()), "default").unwrap();

        args.handle(&mut streams, &facts).unwrap();

        assert_str_eq!(&String::from_utf8_lossy(&streams.out), "id,start,end,note,sheet
2,2021-06-30T10:10:00.000000Z,,hola,default
");

        assert_eq!(
            String::from_utf8_lossy(&streams.err),
            String::new(),
        );
    }

    #[test]
    fn filter_by_match() {
        let mut streams = Streams::fake(b"");
        let facts = Facts::new();

        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 6, 30, 10, 0, 0).unwrap(), None, Some("hola".into()), "default").unwrap();
        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 6, 30, 10, 10, 0).unwrap(), None, Some("adios".into()), "default").unwrap();

        entries_for_display(DisplayOpts {
            start: None,
            end: None,
            sheet: None,
            format: "csv".to_string(),
            ids: true,
            grep: Some("io".parse().unwrap()),
            ..Default::default()
        }, &mut streams, &facts).unwrap();

        assert_eq!(&String::from_utf8_lossy(&streams.out), "id,start,end,note,sheet
2,2021-06-30T10:10:00.000000Z,,adios,default
");

        assert_eq!(
            String::from_utf8_lossy(&streams.err),
            String::new(),
        );
    }

    #[test]
    fn entries_are_grouped_despite_database() {
        let args = Cli {
            common: CommonCli {
                sheet: Some(Sheet::All),
                ..Default::default()
            },
            ..Default::default()
        };
        let mut streams = Streams::fake(b"");
        let facts = Facts::new();
        std::env::set_var("TZ", "CST+6");

        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 6, 30, 10, 0, 0).unwrap(), Some(Utc.with_ymd_and_hms(2021, 6, 30, 11, 0, 0).unwrap()), None, "sheet1").unwrap();
        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 6, 30, 11, 0, 0).unwrap(), Some(Utc.with_ymd_and_hms(2021, 6, 30, 12, 0, 0).unwrap()), None, "sheet2").unwrap();
        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 6, 30, 12, 0, 0).unwrap(), Some(Utc.with_ymd_and_hms(2021, 6, 30, 13, 0, 0).unwrap()), None, "sheet1").unwrap();

        args.handle(&mut streams, &facts).unwrap();

        assert_str_eq!(&String::from_utf8_lossy(&streams.out), "    Day                Start      End        Duration Sheet  Notes
    Wed Jun 30, 2021   04:00:00 - 05:00:00    1:00:00 sheet1
                       05:00:00 - 06:00:00    1:00:00 sheet2
                       06:00:00 - 07:00:00    1:00:00 sheet1
                                              3:00:00
------------------------------------------------------------------
    Total                                     3:00:00

Timesheets: sheet1, sheet2
");
    }

    #[test]
    fn entries_are_grouped_despite_database_full() {
        let args = Cli {
            common: CommonCli {
                sheet: Some(Sheet::Full),
                ..Default::default()
            },
            ..Default::default()
        };
        let mut streams = Streams::fake(b"");
        let facts = Facts::new();
        std::env::set_var("TZ", "CST+6");

        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 6, 30, 10, 0, 0).unwrap(), Some(Utc.with_ymd_and_hms(2021, 6, 30, 11, 0, 0).unwrap()), None, "sheet1").unwrap();
        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 6, 30, 11, 0, 0).unwrap(), Some(Utc.with_ymd_and_hms(2021, 6, 30, 12, 0, 0).unwrap()), None, "_sheet2").unwrap();
        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 6, 30, 12, 0, 0).unwrap(), Some(Utc.with_ymd_and_hms(2021, 6, 30, 13, 0, 0).unwrap()), None, "sheet1").unwrap();

        args.handle(&mut streams, &facts).unwrap();

        assert_str_eq!(&String::from_utf8_lossy(&streams.out), "    Day                Start      End        Duration Sheet   Notes
    Wed Jun 30, 2021   04:00:00 - 05:00:00    1:00:00 sheet1
                       05:00:00 - 06:00:00    1:00:00 _sheet2
                       06:00:00 - 07:00:00    1:00:00 sheet1
                                              3:00:00
-------------------------------------------------------------------
    Total                                     3:00:00

Timesheets: _sheet2, sheet1
");
    }

    #[test]
    fn filter_old_database() {
        std::env::set_var("TZ", "CST+6");

        let args = Cli {
            common: CommonCli {
                format: Some(Formatter::Csv.to_string()),
                end: Some(Utc.with_ymd_and_hms(2021, 6, 29, 13, 0, 0).unwrap()),
                ..Default::default()
            },
            start: Some(Utc.with_ymd_and_hms(2021, 6, 29, 12, 0, 0).unwrap()),
        };
        let mut streams = Streams::fake(b"").with_db(
            SqliteDatabase::from_path("tests/assets/db/test_old_db.db").unwrap()
        );
        let facts = Facts::new();

        // item in database:
        //     start: 2021-06-29 06:26:49.580565
        //     end:   2021-06-29 07:26:52.816747

        args.handle(&mut streams, &facts).unwrap();

        assert_str_eq!(&String::from_utf8_lossy(&streams.out), "id,start,end,note,sheet
1,2021-06-29T12:26:49.580565Z,2021-06-29T13:26:52.816747Z,lets do some rust,default
");

        assert_eq!(
            String::from_utf8_lossy(&streams.err),
            "[WARNING] You are using the old timetrap format, it is advised that you update your database using t migrate. To supress this warning set TIEMPO_SUPRESS_TIMETRAP_WARNING=1\n"
        );
    }

    #[test]
    fn respect_default_formatter() {
        std::env::set_var("TZ", "CST+6");

        let args = Cli::default();
        let mut streams = Streams::fake(b"");
        let facts = Facts::new().with_config(Config {
            default_formatter: Formatter::Ids,
            ..Default::default()
        });

        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 6, 30, 10, 0, 0).unwrap(), None, Some("hola".into()), "default").unwrap();
        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 6, 30, 10, 10, 0).unwrap(), None, Some("hola".into()), "default").unwrap();

        args.handle(&mut streams, &facts).unwrap();

        assert_eq!(&String::from_utf8_lossy(&streams.out), "1 2\n");
        assert_eq!(String::from_utf8_lossy(&streams.err), "");
    }

    #[test]
    fn respect_command_default_formatter() {
        std::env::set_var("TZ", "CST+6");

        let args = Cli::default();
        let mut streams = Streams::fake(b"");
        let facts = Facts::new().with_config(Config {
            commands: CommandsSettings {
                display: BaseCommandSettings {
                    default_formatter: Some(Formatter::Ids),
                },
                ..Default::default()
            },
            ..Default::default()
        });

        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 6, 30, 10, 0, 0).unwrap(), None, Some("hola".into()), "default").unwrap();
        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 6, 30, 10, 10, 0).unwrap(), None, Some("hola".into()), "default").unwrap();

        args.handle(&mut streams, &facts).unwrap();

        assert_eq!(&String::from_utf8_lossy(&streams.out), "1 2\n");
        assert_eq!(String::from_utf8_lossy(&streams.err), "");
    }

    #[test]
    fn test_ambiguous_formater_error() {
        let args = Cli {
            common: CommonCli {
                format: Some(String::from("csve")),
                ..Default::default()
            },
            ..Default::default()
        };
        let mut streams = Streams::fake(b"");
        let facts = Facts::new().with_config(get_config("formatters"));

        let err = args.handle(&mut streams, &facts).unwrap_err();

        assert_str_eq!(err.to_string(), "\
You specified a formatter with name \"csve\", but it is ambiguous. These are the
options:

- csvextra
- csvextraplus

Did you mean one of them?");
    }
}
