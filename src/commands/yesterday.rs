use std::io::{BufRead, Write};

use clap::Args;
use chrono::{Utc, Local, Duration};

use crate::error::Result;
use crate::database::Database;
use crate::io::Streams;
use crate::commands::display::{DisplayOpts, CommonCli, entries_for_display};

use super::{Command, Facts};

#[derive(Default, Args)]
pub struct Cli {
    #[command(flatten)]
    common: CommonCli,
}

impl Command for Cli {
    fn handle<D, I, O, E>(self, streams: &mut Streams<D, I, O, E>, facts: &Facts) -> Result<()>
    where
        D: Database,
        I: BufRead,
        O: Write,
        E: Write,
    {
        let today = facts.now.with_timezone(&Local).date_naive();
        let start = Some((today - Duration::days(1)).and_hms_opt(0, 0, 0).unwrap().and_local_timezone(Utc).unwrap());
        let end = self.common.end.or_else(|| Some(today.and_hms_opt(0, 0, 0).unwrap().and_local_timezone(Utc).unwrap()));

        entries_for_display(
            DisplayOpts {
                start, end,
                sheet: self.common.sheet,
                format: self.common.format.unwrap_or_else(|| {
                    facts
                        .config.commands.yesterday.default_formatter.as_ref()
                        .map(|d| d.to_string())
                        .unwrap_or_else(|| facts.config.default_formatter.to_string())
                }),
                ids: self.common.ids,
                sort: self.common.sort,
                grep: self.common.grep,
            },
            streams,
            facts
        )
    }
}

#[cfg(test)]
mod tests {
    use chrono::{Duration, TimeZone};
    use pretty_assertions::{assert_eq, assert_str_eq};

    use crate::config::{Config, CommandsSettings, BaseCommandSettings};
    use crate::formatters::Formatter;

    use super::*;

    #[test]
    fn returns_yesterday_entries_only() {
        let args = Cli {
            common: CommonCli {
                format: Some(Formatter::Csv.to_string()),
                ..Default::default()
            },
        };
        let mut streams = Streams::fake(b"");
        let two_days_ago = Local::now().date_naive() - Duration::days(2);
        let yesterday = Local::now().date_naive() - Duration::days(1);
        let today = Local::now().date_naive();
        let facts = Facts::new();

        streams.db.entry_insert(two_days_ago.and_hms_opt(1, 2, 3).unwrap().and_local_timezone(Utc).unwrap(), None, None, "default").unwrap();
        streams.db.entry_insert(yesterday.and_hms_opt(1, 2, 3).unwrap().and_local_timezone(Utc).unwrap(), None, Some("This!".into()), "default").unwrap();
        streams.db.entry_insert(today.and_hms_opt(1, 2, 3).unwrap().and_local_timezone(Utc).unwrap(), None, None, "default").unwrap();

        args.handle(&mut streams, &facts).unwrap();

        assert_str_eq!(&String::from_utf8_lossy(&streams.out), &format!("id,start,end,note,sheet
2,{},,This!,default
", yesterday.and_hms_opt(1, 2, 3).unwrap().and_local_timezone(Utc).unwrap().to_rfc3339_opts(chrono::SecondsFormat::Micros, true)));

        assert_eq!(
            String::from_utf8_lossy(&streams.err),
            String::new(),
        );
    }

    #[test]
    fn respect_default_formatter() {
        std::env::set_var("TZ", "CST+6");

        let args = Cli::default();
        let mut streams = Streams::fake(b"");
        let now = Utc.with_ymd_and_hms(2021, 7, 1, 10, 0, 0).unwrap();
        let facts = Facts::new().with_config(Config {
            default_formatter: Formatter::Ids,
            ..Default::default()
        }).with_now(now);

        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 6, 30, 10, 0, 0).unwrap(), None, Some("hola".into()), "default").unwrap();
        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 6, 30, 10, 10, 0).unwrap(), None, Some("hola".into()), "default").unwrap();

        args.handle(&mut streams, &facts).unwrap();

        assert_eq!(&String::from_utf8_lossy(&streams.out), "1 2\n");
        assert_eq!(String::from_utf8_lossy(&streams.err), "");
    }

    #[test]
    fn respect_command_default_formatter() {
        std::env::set_var("TZ", "CST+6");

        let args = Cli::default();
        let mut streams = Streams::fake(b"");
        let now = Utc.with_ymd_and_hms(2021, 7, 1, 10, 0, 0).unwrap();
        let facts = Facts::new().with_config(Config {
            commands: CommandsSettings {
                yesterday: BaseCommandSettings {
                    default_formatter: Some(Formatter::Ids),
                },
                ..Default::default()
            },
            ..Default::default()
        }).with_now(now);

        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 6, 30, 10, 0, 0).unwrap(), None, Some("hola".into()), "default").unwrap();
        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 6, 30, 10, 10, 0).unwrap(), None, Some("hola".into()), "default").unwrap();

        args.handle(&mut streams, &facts).unwrap();

        assert_eq!(&String::from_utf8_lossy(&streams.out), "1 2\n");
        assert_eq!(String::from_utf8_lossy(&streams.err), "");
    }
}
