use std::io::{BufRead, Write};

use clap::Args;

use crate::error::Result;
use crate::database::Database;
use crate::interactive::{ask, confirm_deletion};
use crate::io::Streams;

use super::{Command, Facts};

/// Delete an entry or an entire sheet
#[derive(Args)]
#[group(required=true)]
pub struct Cli {
    /// Delete entry with this ID instead of sheet
    #[arg(long, value_name="ID", exclusive=true)]
    id: Option<u64>,

    /// Delete an entire sheet by its name
    #[arg(short, long, value_name="SHEET", exclusive=true)]
    sheet: Option<String>,

    /// Delete the last entry of the current sheet
    #[arg(short, long, exclusive=true)]
    last: bool,
}

// This internal structure allows for a type-consistent way of handling the CLI
// which is composed of three mutually exclusive possible arguments.
#[derive(Debug)]
enum InternalArgs {
    Id(u64),
    Sheet(String),
    Last,
}

impl From<Cli> for InternalArgs {
    fn from(cli: Cli) -> InternalArgs {
        if let Some(id) = cli.id {
            InternalArgs::Id(id)
        } else if cli.last {
            InternalArgs::Last
        } else if let Some(sheet) = cli.sheet {
            InternalArgs::Sheet(sheet)
        } else {
            unreachable!()
        }
    }
}

impl Command for Cli {
    fn handle<D, I, O, E>(self, streams: &mut Streams<D, I, O, E>, facts: &Facts) -> Result<()>
    where
        D: Database,
        I: BufRead,
        O: Write,
        E: Write,
    {
        InternalArgs::from(self).handle(streams, facts)
    }
}

impl Command for InternalArgs {
    fn handle<D, I, O, E>(self, streams: &mut Streams<D, I, O, E>, facts: &Facts) -> Result<()>
    where
        D: Database,
        I: BufRead,
        O: Write,
        E: Write,
    {
        match self {
            InternalArgs::Id(id) => {
                if let Some(entry) = streams.db.entry_by_id(id)? {
                    confirm_deletion(streams, entry, facts.now)?;
                } else {
                    writeln!(streams.out, "There's no entry with id {}. Someone found it before we did.", id)?;
                }
            }
            InternalArgs::Last => {
                let current_sheet = streams.db.current_sheet()?;
                if let Some(entry) = streams.db.last_entry_of_sheet(&current_sheet)? {
                    confirm_deletion(streams, entry, facts.now)?;
                } else {
                    writeln!(streams.out, "Nothing to delete")?;
                }
            }
            InternalArgs::Sheet(sheet) => {
                let n = streams.db.entries_by_sheet(&sheet, None, None)?.len();

                if ask(streams, &format!("are you sure you want to delete {} entries on sheet \"{}\"?", n, sheet))? {
                    streams.db.delete_entries_in_sheet(&sheet)?;
                    writeln!(streams.out, "They're gone")?;
                } else {
                    writeln!(streams.out, "Don't worry, they're still there")?;
                }
            }
        }

        Ok(())
    }
}
