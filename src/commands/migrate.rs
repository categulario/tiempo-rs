use std::io::{BufRead, Write};
use std::path::PathBuf;
use std::fs::create_dir_all;

use clap::Args;
use directories::ProjectDirs;

use crate::error::Result;
use crate::database::{Database, SqliteDatabase, DBVersion};
use crate::io::Streams;
use crate::old::local_to_utc_vec;
use crate::error::Error::{NoHomeDir, GenericFailure};
use crate::config::Config;

use super::{Command, Facts};

/// Migrates your existing timetrap database to tiempo's format.
///
/// The database will be converted from local times to UTC. Databases already in
/// the new format will not be migrated. If a database already exists in the
/// specified location the command will warn and exit. Pass --db-recommended to
/// store the new database at the recommended location (in unix-like systems
/// under ~/.config/tiempo/).
///
/// When finished you will be presented with a recommended settings file that
/// you can use or just take a portion to update your existing settings file.
/// You will also see a list of further steps to take to finish the migration.
#[derive(Default, Args)]
pub struct Cli {
    /// Use the recommended database location for the new database. Parent
    /// directories will be created if necessary.
    #[arg(long, conflicts_with="db")]
    db_recommended: bool,

    /// Path where the new database will be located. Must include filename.
    /// Parent directories will be created if necessary.
    #[arg(long, required=true, value_name="PATH")]
    db: Option<PathBuf>,
}

impl Command for Cli {
    fn handle<D, I, O, E>(self, streams: &mut Streams<D, I, O, E>, facts: &Facts) -> Result<()>
    where
        D: Database,
        I: BufRead,
        O: Write,
        E: Write,
    {
        let db_location = &facts.config.database_file;
        let config_location = facts.config.path.as_ref().unwrap();

        if let DBVersion::Version(x) = streams.db.version()? {
            return Err(GenericFailure(format!("\
You are trying to migrate a database already in the new format (v{x}) located at

    {db_location:?}

Specified in config located at

    {config_location:?}

Nothing to do.\
")));
        }

        let entries = local_to_utc_vec(streams.db.entries_full(None, None)?)?;

        let default_cofig_dir = if let Some(project_dirs) = ProjectDirs::from("xyz", "categulario", "tiempo") {
            project_dirs.config_dir().to_owned()
        } else {
            return Err(NoHomeDir);
        };

        let db_path = if self.db_recommended {
            let mut path = default_cofig_dir.clone();
            path.push("database.sqlite3");
            path
        } else {
            self.db.unwrap()
        };

        if db_path.exists() {
            return Err(GenericFailure(format!("A database already exists in the specified location ({db_path:?})")));
        }

        create_dir_all(db_path.parent().unwrap())?;

        let mut new_db = SqliteDatabase::from_path_or_create(&db_path)?;

        for entry in entries {
            new_db.entry_insert_full(entry)?;
        }

        let new_config = Config {
            database_file: db_path.clone(),
            ..facts.config.clone()
        };

        let recommended_config_path = {
            let mut path = default_cofig_dir.clone();
            path.push("config.toml");
            path
        };

        writeln!(streams.out, "\
Database exported to new location: {db_path:?}
Entries where converted from local time to UTC so you can switch between
timezones around the world with confidence.

Recommended config:

{}

Further steps:

* [ ] store above config at {recommended_config_path:?} or copy `database_file`
      to your existing config.
* [ ] unset TIEMPO_SUPRESS_TIMETRAP_WARNING env var if set.
* [ ] unset TIEMPO_CONFIG or TIMETRAP_CONFIG_FILE if using recommended config path.
      You can also keep your old config, just adjust the path to the database.
* [ ] translate custom formatters and set formatter_search_paths properly in the config. See
      https://tiempo.categulario.xyz/#custom-formatters for reference.
* [ ] read the changelog and the docs to learn features that make tiempo unique
      changelog: https://gitlab.com/categulario/tiempo-rs/-/blob/main/CHANGELOG.md
      docs: https://tiempo.categulario.xyz/
        ", toml::to_string(&new_config).unwrap())?;

        Ok(())
    }
}
