use std::io::{BufRead, Write};

use clap::Args;
use chrono::{DateTime, Utc};

use crate::database::Database;
use crate::error::Result;
use crate::editor;
use crate::commands::{Command, Facts};
use crate::timeparse::parse_time;
use crate::old::{time_or_warning, warn_if_needed};
use crate::io::Streams;

/// Start an activity in the current timesheet
#[derive(Default, Args)]
pub struct Cli {
    /// Use this time instead of now
    #[arg(long, value_name="TIME", value_parser=parse_time)]
    pub at: Option<DateTime<Utc>>,

    /// Text describing the activity to start
    #[arg(value_name="NOTE")]
    pub note: Option<String>,
}

impl Command for Cli {
    fn handle<D, I, O, E>(self, streams: &mut Streams<D, I, O, E>, facts: &Facts) -> Result<()>
    where
        D: Database,
        I: BufRead,
        O: Write,
        E: Write,
    {
        let start = self.at.unwrap_or(facts.now);
        let sheet = streams.db.current_sheet()?;

        if streams.db.running_entry(&sheet)?.is_some() {
            writeln!(streams.out, "Timer is already running for sheet '{}'", sheet)?;

            return Ok(());
        }

        let note = if let Some(note) = self.note {
            Some(note.trim().to_owned())
        } else if !facts.config.require_note {
            None
        } else {
            Some(editor::get_string(facts.config.note_editor.as_deref(), None)?)
        };

        let (start, needs_warning) = time_or_warning(start, &streams.db)?;

        streams.db.entry_insert(start, None, note, &sheet)?;

        writeln!(streams.out, "Checked into sheet \"{}\".", sheet)?;

        warn_if_needed(&mut streams.err, needs_warning, &facts.env)?;

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use pretty_assertions::assert_eq;
    use chrono::{TimeZone, Local};

    use crate::database::SqliteDatabase;
    use crate::config::Config;

    use super::*;

    #[test]
    fn handles_new_entry() {
        let args = Cli {
            at: None,
            note: Some("hola".into()),
        };
        let mut streams = Streams::fake(b"");
        let facts = Facts::new();

        assert_eq!(streams.db.entries_full(None, None).unwrap().len(), 0);

        args.handle(&mut streams, &facts).unwrap();

        let e = streams.db.entries_full(None, None).unwrap().into_iter().next().unwrap();

        assert_eq!(e.note, Some("hola".into()));
        assert_eq!(e.start, facts.now);
        assert_eq!(e.end, None);
        assert_eq!(e.sheet, "default".to_owned());

        assert_eq!(&String::from_utf8_lossy(&streams.out), "Checked into sheet \"default\".\n");
        assert_eq!(&String::from_utf8_lossy(&streams.err), "");
    }

    #[test]
    fn test_handles_already_running_entry() {
        let args = Cli {
            at: None,
            note: Some("hola".into()),
        };
        let mut streams = Streams::fake(b"");
        let facts = Facts::new();

        streams.db.entry_insert(facts.now, None, None, "default").unwrap();

        assert_eq!(streams.db.entries_full(None, None).unwrap().len(), 1);

        args.handle(&mut streams, &facts).unwrap();

        assert_eq!(streams.db.entries_full(None, None).unwrap().len(), 1);

        assert_eq!(
            &String::from_utf8_lossy(&streams.out),
            "Timer is already running for sheet 'default'\n"
        );
    }

    #[test]
    fn no_note_and_no_mandatory_leaves_none() {
        let args = Cli::default();
        let mut streams = Streams::fake(b"");
        let facts = Facts::new().with_config(Config {
            require_note: false,
            ..Default::default()
        });

        assert_eq!(streams.db.entries_full(None, None).unwrap().len(), 0);

        args.handle(&mut streams, &facts).unwrap();

        let e = streams.db.entries_full(None, None).unwrap().into_iter().next().unwrap();

        assert_eq!(e.note, None);
        assert_eq!(e.start, facts.now);
        assert_eq!(e.end, None);
        assert_eq!(e.sheet, "default".to_owned());
    }

    #[test]
    fn warns_if_old_database() {
        let args = Cli {
            at: None,
            note: Some("hola".into()),
        };
        let mut streams = Streams::fake(b"").with_db({
            let mut db = SqliteDatabase::from_memory().unwrap();

            db.init_old().unwrap();

            db
        });
        let facts = Facts::new();

        assert_eq!(streams.db.entries_full(None, None).unwrap().len(), 0);

        args.handle(&mut streams, &facts).unwrap();

        let e = streams.db.entries_full(None, None).unwrap().into_iter().next().unwrap();

        assert_eq!(e.note, Some("hola".into()));
        assert_eq!(e.start, Utc.from_utc_datetime(&facts.now.with_timezone(&Local).naive_local()));
        assert_eq!(e.end, None);
        assert_eq!(e.sheet, "default".to_owned());

        assert_eq!(&String::from_utf8_lossy(&streams.out), "Checked into sheet \"default\".\n");
        assert_eq!(&String::from_utf8_lossy(&streams.err),
            "[WARNING] You are using the old timetrap format, it is advised that \
            you update your database using t migrate. To supress this warning set TIEMPO_SUPRESS_TIMETRAP_WARNING=1\n");
    }

    #[test]
    fn notes_are_trimmed() {
        let args = Cli {
            at: None,
            note: Some("hola\n".into()),
        };
        let mut streams = Streams::fake(b"");
        let facts = Facts::new();

        assert_eq!(streams.db.entries_full(None, None).unwrap().len(), 0);

        args.handle(&mut streams, &facts).unwrap();

        let e = streams.db.entries_full(None, None).unwrap().into_iter().next().unwrap();

        assert_eq!(e.note, Some("hola".into()));
        assert_eq!(e.start, facts.now);
        assert_eq!(e.end, None);
        assert_eq!(e.sheet, "default".to_owned());

        assert_eq!(&String::from_utf8_lossy(&streams.out), "Checked into sheet \"default\".\n");
        assert_eq!(&String::from_utf8_lossy(&streams.err), "");
    }
}
