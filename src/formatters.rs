use std::str::FromStr;
use std::io::Write;
use std::fmt;
use std::cmp::Ordering;

use serde::{Serialize, Deserialize};

use crate::models::Entry;
use crate::commands::{Facts, display::Sort};
use crate::config::Config;
use crate::error::{Error, Result};

use strum::{EnumIter, IntoEnumIterator};
use is_executable::IsExecutable;

pub mod text;
pub mod csv;
pub mod json;
pub mod ids;
pub mod ical;
pub mod custom;
pub mod chart;
pub mod activities;
pub mod csvactivities;

#[derive(Debug, Clone, Default, PartialEq, Eq, Serialize, Deserialize, EnumIter)]
#[serde(rename_all = "lowercase")]
#[strum(serialize_all = "lowercase")]
pub enum Formatter {
    #[default]
    Text,
    Csv,
    Json,
    Ids,
    Ical,
    Chart,
    Activities,
    CsvActivities,
    Custom(String),
}

impl Formatter {
    /// Prints the given entries to the specified output device.
    ///
    /// the current time is given as the `now` argument and the offset from UTC
    /// to the local timezone is given in `offset` to prevent this function from
    /// using a secondary effect to retrieve the time and conver dates. This
    /// also makes it easier to test.
    pub fn print_formatted<O, E>(&self, entries: Vec<Entry>, out: &mut O, err: &mut E, facts: &Facts, ids: bool, sort: Sort) -> Result<()>
    where
        O: Write,
        E: Write,
    {
        match &self {
            Formatter::Text => text::print_formatted(entries, out, facts, ids)?,
            Formatter::Csv => csv::print_formatted(entries, out)?,
            Formatter::Json => json::print_formatted(entries, out)?,
            Formatter::Ids => ids::print_formatted(entries, out)?,
            Formatter::Ical => ical::print_formatted(entries, out, facts.now)?,
            Formatter::Chart => chart::print_formatted(entries, out, facts)?,
            Formatter::Activities => activities::print_formatted(entries, out, facts, sort)?,
            Formatter::CsvActivities => csvactivities::print_formatted(entries, out, facts, sort)?,
            Formatter::Custom(name) => custom::print_formatted(name, entries, out, err, facts)?,
        }

        Ok(())
    }
}

impl FromStr for Formatter {
    type Err = Error;

    fn from_str(s: &str) -> Result<Formatter> {
        Ok(match s.to_lowercase().as_str() {
            "text" => Formatter::Text,
            "csv" => Formatter::Csv,
            "json" => Formatter::Json,
            "ids" => Formatter::Ids,
            "ical" => Formatter::Ical,
            "chart" => Formatter::Chart,
            custom_format => Formatter::Custom(custom_format.into()),
        })
    }
}

impl fmt::Display for Formatter {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        let s = match self {
            Formatter::Text => "text",
            Formatter::Csv => "csv",
            Formatter::Json => "json",
            Formatter::Ids => "ids",
            Formatter::Ical => "ical",
            Formatter::Chart => "chart",
            Formatter::Activities => "activities",
            Formatter::CsvActivities => "csvactivities",
            Formatter::Custom(s) => s,
        };

        write!(formatter, "{s}")
    }
}

impl PartialOrd for Formatter {
    fn partial_cmp(&self, rhs: &Self) -> Option<Ordering> {
        Some(self.cmp(rhs))
    }
}

impl Ord for Formatter {
    fn cmp(&self, rhs: &Self) -> Ordering {
        self.to_string().cmp(&rhs.to_string())
    }
}

#[derive(Debug, PartialEq, Eq)]
pub enum MatchResult {
    Found(Formatter),
    Ambiguous(Vec<String>),
    NotFound(String),
}

/// Gets Formatter from prefix
///
/// from Formatter variants try to match with starts_with.
pub(crate) fn autocomplete(mut formatters: Vec<Formatter>, s: &str) -> MatchResult {
    formatters.sort();

    let mut formats = Vec::new();

    for formatter in formatters {
        let name = formatter.to_string();
        if name.starts_with(&s.to_lowercase()) {
            formats.push(formatter);

            // break on exact name match. Since formatters come sorted by name
            // this will not prevent ambiguity from being detected if a prefix
            // is common to two formatters but not an exact match (exact matches
            // come first in lexicographic order).
            if name == s {
                break;
            }
        }
    }

    match &formats[..] {
        [] => MatchResult::NotFound(s.to_string()),
        [f] => MatchResult::Found(f.to_owned()),
        formats => MatchResult::Ambiguous(formats.iter().map(|f| f.to_string()).collect()),
    }
}

pub(crate) fn get_formatters(config: &Config) -> Result<Vec<Formatter>> {
    let mut formatters: Vec<_> = Formatter::iter()
        .filter(|f| !matches!(f, Formatter::Custom(_)))
        .collect();

    for path in &config.formatter_search_paths {
        if path.exists() {
            for entry in path.read_dir().map_err(|error| Error::ReadingFormatters { path: path.clone(), error })? {
                let path = entry.map_err(|error| Error::ReadingFormatters { path: path.clone(), error })?.path();
                if path.is_file() && path.is_executable() {
                    formatters.push(Formatter::Custom(path.file_name().unwrap().to_string_lossy().into_owned()))
                }
            }
        }
    }

    Ok(formatters)
}

#[cfg(test)]
pub mod tests {
    use std::path::PathBuf;

    use super::*;

    pub fn get_config(dir: &str) -> Config {
        let formatters_path = PathBuf::from(file!())
            .parent().unwrap().parent().unwrap().join("tests/assets/".to_owned() + dir);

        Config {
            formatter_search_paths: vec![formatters_path],
            ..Default::default()
        }
    }

    #[test]
    fn get_formatters_works() {
        let mut formatters = get_formatters(&get_config("formatters")).unwrap();
        let test = vec![
            Formatter::Activities, Formatter::Chart,
            Formatter::Custom("costs".into()), Formatter::Csv,
            Formatter::CsvActivities,
            Formatter::Custom("csvextra".into()),
            Formatter::Custom("csvextraplus".into()),
            Formatter::Custom("earnings".into()),
            Formatter::Ical, Formatter::Ids, Formatter::Json, Formatter::Text,
        ];
        formatters.sort();
        assert_eq!(formatters, test);
    }

    #[test]
    fn found_autocomplete() {
        let format = autocomplete(get_formatters(&get_config("formatters")).unwrap(), "t");
        assert_eq!(format, MatchResult::Found(Formatter::Text));
    }

    #[test]
    fn found_autocomplete_custom() {
        let format = autocomplete(get_formatters(&get_config("formatters")).unwrap(), "earn");
        assert_eq!(format, MatchResult::Found(Formatter::Custom("earnings".to_string())));
    }

    #[test]
    fn ambiguous_autocomplete() {
        let format = autocomplete(get_formatters(&get_config("formatters")).unwrap(), "i");
        assert_eq!(format, MatchResult::Ambiguous(vec!["ical".to_string(), "ids".to_string()]));
    }

    #[test]
    fn not_found_autocomplete() {
        let format = autocomplete(get_formatters(&get_config("formatters")).unwrap(), "fail");
        assert_eq!(format, MatchResult::NotFound("fail".to_string()));
    }

    #[test]
    fn formatter_that_is_a_prefix_is_not_ambiguous() {
        let format = autocomplete(get_formatters(&get_config("formatters")).unwrap(), "csv");
        assert_eq!(format, MatchResult::Found(Formatter::Csv));
    }

    #[test]
    fn formatter_that_is_a_prefix_is_not_ambiguous_in_custom() {
        let formatters = vec![
            Formatter::Custom("csvextraplus".into()),
            Formatter::Custom("csvextra".into()),
        ];
        let format = autocomplete(formatters, "csvextra");
        assert_eq!(format, MatchResult::Found(Formatter::Custom("csvextra".into())));
    }

    #[test]
    fn formatter_dir_doesnt_exists() {
        let format = autocomplete(get_formatters(&get_config("formatters.no")).unwrap(), "csv");
        assert_eq!(format, MatchResult::Found(Formatter::Csv));
    }
}
