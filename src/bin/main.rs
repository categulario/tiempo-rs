use std::process::exit;
use std::io;

use clap::Parser;
use chrono::Utc;

use tiempo::error;
use tiempo::database::SqliteDatabase;
use tiempo::env::Env;
use tiempo::config::Config;
use tiempo::commands::{Facts, Command};
use tiempo::io::Streams;
use tiempo::cli::{Cli, Subcli};

fn error_trap() -> error::Result<()> {
    let env = Env::read();
    let facts = Facts {
        config: Config::read(&env)?,
        env,
        now: Utc::now(),
    };
    let matches = Cli::parse();

    let mut streams = Streams {
        db: SqliteDatabase::from_path_or_create(&facts.config.database_file)?,
        r#in: io::BufReader::new(io::stdin()),
        out: io::stdout(),
        err: io::stderr(),
    };

    match matches.subcommand {
        Subcli::Archive(c) => c.handle(&mut streams, &facts),
        Subcli::Backend(b) => b.handle(&mut streams, &facts),
        Subcli::Configure(b) => b.handle(&mut streams, &facts),
        Subcli::Display(b) => b.handle(&mut streams, &facts),
        Subcli::Edit(b) => b.handle(&mut streams, &facts),
        Subcli::Kill(b) => b.handle(&mut streams, &facts),
        Subcli::List(x) => x.handle(&mut streams, &facts),
        Subcli::Migrate(x) => x.handle(&mut streams, &facts),
        Subcli::Month(x) => x.handle(&mut streams, &facts),
        Subcli::Now(x) => x.handle(&mut streams, &facts),
        Subcli::Out(x) => x.handle(&mut streams, &facts),
        Subcli::In(x) => x.handle(&mut streams, &facts),
        Subcli::Resume(x) => x.handle(&mut streams, &facts),
        Subcli::Sheet(x) => x.handle(&mut streams, &facts),
        Subcli::Today(x) => x.handle(&mut streams, &facts),
        Subcli::Week(x) => x.handle(&mut streams, &facts),
        Subcli::Yesterday(x) => x.handle(&mut streams, &facts),
    }
}

fn main() {
    if let Err(e) = error_trap() {
        eprintln!("{}", e);
        exit(1);
    }
}
