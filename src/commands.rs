use std::io::{BufRead, Write};

use chrono::{DateTime, Utc};

use crate::error::Result;
use crate::database::Database;
use crate::config::Config;
use crate::io::Streams;
use crate::env::Env;

pub mod archive;
pub mod backend;
pub mod configure;
pub mod display;
pub mod edit;
pub mod kill;
pub mod list;
pub mod migrate;
pub mod month;
pub mod now;
pub mod out;
pub mod r#in;
pub mod resume;
pub mod sheet;
pub mod today;
pub mod week;
pub mod yesterday;

pub struct Facts {
    pub now: DateTime<Utc>,
    pub config: Config,
    pub env: Env,
}

impl Facts {
    pub fn new() -> Facts {
        Facts {
            now: Utc::now(),
            config: Default::default(),
            env: Default::default(),
        }
    }

    pub fn with_config(self, config: Config) -> Facts {
        Facts {
            config,
            ..self
        }
    }

    pub fn with_now(self, now: DateTime<Utc>) -> Facts {
        Facts {
            now,
            ..self
        }
    }
}

impl Default for Facts {
    fn default() -> Facts {
        Facts::new()
    }
}

pub trait Command {
    fn handle<D: Database, I: BufRead, O: Write, E: Write>(self, streams: &mut Streams<D, I, O, E>, facts: &Facts) -> Result<()>;
}
