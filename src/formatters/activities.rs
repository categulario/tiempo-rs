use std::io::Write;
use std::collections::HashMap;
use std::cmp::Reverse;

use chrono::{DateTime, Utc, Duration};

use crate::error::Result;
use crate::models::Entry;
use crate::commands::{Facts, display::Sort};
use crate::tabulate::{Tabulate, Col, Align};
use crate::formatters::text::format_duration;

pub fn print_formatted<W: Write>(entries: Vec<Entry>, out: &mut W, facts: &Facts, sort: Sort) -> Result<()> {
    let mut uniques = HashMap::new();

    struct GroupedEntry {
        note: String,
        last_start: DateTime<Utc>,
        accumulated_time: Duration,
    }

    // From all possible entries belonging to this sheet keep only those with a
    // note
    let entries_with_notes = entries
        .into_iter()
        .filter_map(|e| e.note.map(|n| GroupedEntry {
            note: n,
            last_start: e.start,
            accumulated_time: e.end.unwrap_or(facts.now) - e.start,
        }));

    // iterate over the entries with a note and group them into `uniques`
    // accumulating their elapsed times and recording the last time it was
    // started
    for entry in entries_with_notes {
        let e = uniques.entry(entry.note.clone()).or_insert(GroupedEntry {
            accumulated_time: Duration::seconds(0),
            ..entry
        });

        if entry.last_start > e.last_start {
            e.last_start = entry.last_start;
        }

        e.accumulated_time += entry.accumulated_time;
    }

    // turn uniques into a vector and sort it by the time it was last started
    let uniques = {
        let mut uniques: Vec<_> = uniques.into_values().collect();

        match sort {
            Sort::NoteAsc => uniques.sort_unstable_by_key(|e| e.note.clone()),
            Sort::NoteDsc => uniques.sort_unstable_by_key(|e| Reverse(e.note.clone())),
            Sort::TimeAsc => uniques.sort_unstable_by_key(|e| e.accumulated_time),
            Sort::TimeDsc => uniques.sort_unstable_by_key(|e| Reverse(e.accumulated_time)),
            Sort::LastStartedAsc => uniques.sort_unstable_by_key(|e| e.last_start),
            Sort::LastStartedDsc => uniques.sort_unstable_by_key(|e| Reverse(e.last_start)),
        }

        uniques
    };

    let formatter = timeago::Formatter::new();

    // Create a table for nicer output
    let mut table = Tabulate::with_columns(vec![
        Col::new(), // note
        Col::new().and_alignment(Align::Right), // acumulated time
        Col::new().min_width(13).and_alignment(Align::Right), // last started
    ]);

    table.feed(vec!["Note", "Time", "Last started"]);
    table.separator(' ');

    let mut total_time = Duration::seconds(0);

    for entry in uniques.iter() {
        let ago = formatter.convert_chrono(entry.last_start, facts.now);

        total_time += entry.accumulated_time;

        table.feed(vec![
            entry.note.clone(),
            format_duration(entry.accumulated_time),
            ago,
        ]);
    }

    table.separator(' ');
    table.feed(vec!["Total", &format_duration(total_time)]);

    write!(out, "{}", table.print(false))?;

    Ok(())
}

#[cfg(test)]
mod tests {
}
