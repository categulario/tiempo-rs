# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information
from pathlib import Path
from tomlkit import parse

# Make the Cargo.toml file the only source for 'version'
_cargo_toml = Path(__file__).resolve().parent.parent.parent / 'Cargo.toml'

project = 'Tiempo'
copyright = '2022, Categulario'
author = 'Categulario, Perrotuerto'
release = str(parse(open(_cargo_toml).read())['package']['version'])

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = []

templates_path = ['_templates']
exclude_patterns = []



# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'furo'
html_static_path = ['_static']
html_logo = "_static/logo.png"
html_title = "Tiempo docs"
html_theme_options = {
    "source_repository": "https://gitlab.com/categulario/tiempo-rs/",
    "source_branch": "main",
    "source_directory": "docs/source/",
}
html_css_files = ['custom.css']
